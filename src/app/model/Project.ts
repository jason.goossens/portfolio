export interface Project {
  name: string;
  title: string;
  description: string;
  thumbnail: string;
  images: {
    alt: string;
    url: string;
  }[],
  technologies: {
    type:string;
    name: string;
  }[],
  VCS: {
    name: string;
    url: string;
  }[];
}
