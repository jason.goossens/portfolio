import {Routes} from '@angular/router';
import {HomeComponent} from "./content/home/home.component";
import {AboutComponent} from "./content/about/about.component";
import {SkillsComponent} from "./skills/skills.component";
import {ProjectsComponent} from "./projects/projects.component";
import {ProjectComponent} from "./project/project.component";
import {ContactComponent} from "./contact/contact.component";

export const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "about", component: AboutComponent},
  {path: "skills", component: SkillsComponent},
  {path: "projects", component: ProjectsComponent},
  {path: "projects/:name", component: ProjectComponent},
  {path: "contact", component: ContactComponent}
];

