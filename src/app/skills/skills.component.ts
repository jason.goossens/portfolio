import {Component, OnInit} from '@angular/core';
import {AngularD3CloudModule} from 'angular-d3-cloud';
import * as skillsFile from '../../assets/data/skills.json';
import {Skill} from "../model/Skill";
import {KeyValuePipe, NgForOf, TitleCasePipe} from "@angular/common";

@Component({
  selector: 'app-skills',
  standalone: true,
  imports: [AngularD3CloudModule, NgForOf, KeyValuePipe, TitleCasePipe],
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  data!: any;
  rotateFunction: (datum: any, index: number) => number;
  width: number = 500;
  skillsSelect!: { [type: string]: string[] };

  constructor() {
    this.rotateFunction = this.getRandomRotation.bind(this);
  }

  ngOnInit(): void {
    this.generateWordMap();
    this.generateSkillsSelect();
  }

  generateWordMap() {
    this.width = window.innerWidth / 2;
    this.data = skillsFile.skills
      .map((word) => {
        return {
          text: word.name,
          value: 10 + Math.random() * window.innerWidth / 32,
          rotate: this.rotateFunction
        };
      });
  }

  getRandomRotation(): number {
    const rotations = [-55, -50, -45, -30, -15, 5, 30, 45, 50, 55];
    return rotations[Math.floor(Math.random() * rotations.length)];
  }

  generateSkillsSelect() {
    let finalSkillsArray: { [type: string]: string[] } = {};

    skillsFile.skills.forEach((skill: Skill) => {
      if (!finalSkillsArray[skill.type]) {
        finalSkillsArray[skill.type] = [];
      }
      finalSkillsArray[skill.type].push(skill.name);

      this.skillsSelect = finalSkillsArray;
    })
  }
}
