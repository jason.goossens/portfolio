import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Project} from "../model/Project";
import * as projectsFile from '../../assets/data/projects.json';
import {AsyncPipe, NgForOf, NgIf} from "@angular/common";
import {CarouselComponent} from "../util/carousel/carousel.component";

@Component({
  selector: 'app-project',
  standalone: true,
  imports: [
    AsyncPipe,
    CarouselComponent,
    NgIf,
    NgForOf
  ],
  templateUrl: './project.component.html',
  styleUrl: './project.component.css'
})
export class ProjectComponent implements OnInit {
  project?: Project;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.project = projectsFile.projects.find((p: Project) => p.name === params['name']);
    });
  }

  setBadgeColour(type: string) {
    switch (type) {
      case "language":
        return "primary";
      case "framework":
        return "success";
      case "library":
        return "danger";
      case "database":
        return "warning";
      case "ci/cd":
        return "info";

    }
    return "secondary";
  }
}
