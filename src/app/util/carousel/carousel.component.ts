import {Component, Input} from '@angular/core';
import {NgForOf} from "@angular/common";
import {NgbCarousel, NgbSlide, NgbSlideEvent} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [
    NgForOf,
    NgbCarousel,
    NgbSlide
  ],
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.css'
})
export class CarouselComponent {
  @Input() name?: string;
  @Input() images: any;

  onSlide($event: NgbSlideEvent) {
    console.log($event);
  }
}
