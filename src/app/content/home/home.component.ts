import {Component} from '@angular/core';
import {RouterLink} from "@angular/router";
import * as homeFaceImagesList from '../../../assets/data/homeFaceImagesList.json';

@Component({
  selector: 'app-home',

  standalone: true,
  imports: [
    RouterLink

  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  homeFaceImages!: string[];

  constructor() {
    this.homeFaceImages = homeFaceImagesList.files.map(f => f.path);
  }

  getRandomHomePhoto(): string {
    return this.homeFaceImages[Math.floor(Math.random() * this.homeFaceImages.length)];

  }
}
