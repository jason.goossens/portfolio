import {Component} from '@angular/core';
import * as projectsFile from '../../assets/data/projects.json';
import {NgForOf} from "@angular/common";
import {Project} from "../model/Project";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [
    NgForOf,
    RouterLink
  ],
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.css'
})
export class ProjectsComponent {
  projectsList!: Project[];

  constructor() {
    this.projectsList = projectsFile.projects;
  }
}
